package de.digisocken.wiwidie;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {
    public static final int LOGLENGTH   = 500;
    public static final int LISTEN_PORT = 64111;

    private static final String SERVICE_NAME = "_wiwidie";
    private static final String SERVICE_TYPE = "_tcp";
    private static final String NAME_RECORD_FIELD = "name";

    public static SharedPreferences mPreferences;

    private WifiP2pManager.Channel mChannel;
    private WifiP2pManager mWifiP2pManager;
    private IntentFilter mWifiP2pChangeIntentFilter;
    private WifiP2pStateChangeReceiver mWifiP2pStateChangeReceiver;
    public TextView logging;
    public EditText nameView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        logging  = (TextView) findViewById(R.id.logging);
        nameView = (EditText) findViewById(R.id.name);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (!mPreferences.contains(NAME_RECORD_FIELD)) {
            mPreferences.edit().putString(NAME_RECORD_FIELD, nameView.getText().toString()).apply();
        } else {
            nameView.setText(mPreferences.getString(NAME_RECORD_FIELD, "Simone 123"));
        }

        mWifiP2pChangeIntentFilter = new IntentFilter();
        //  Indicates a change in the Wi-Fi P2P status.
        mWifiP2pChangeIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        mWifiP2pChangeIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        mWifiP2pChangeIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        mWifiP2pChangeIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        mWifiP2pStateChangeReceiver = new WifiP2pStateChangeReceiver();

    }

    @Override
    protected void onResume() {
        super.onResume();
        nameView.setText(mPreferences.getString(NAME_RECORD_FIELD, "Simone 123"));
        if (mChannel == null) {
            initializeService();
        }
        registerReceiver(mWifiP2pStateChangeReceiver, mWifiP2pChangeIntentFilter);
        runDiscovery();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPreferences.edit().putString(NAME_RECORD_FIELD, nameView.getText().toString()).apply();
        unregisterReceiver(mWifiP2pStateChangeReceiver);
    }


    public void initializeService() {
        Map<String, String> record = new HashMap<>();
        record.put(NAME_RECORD_FIELD, nameView.getText().toString());
        final WifiP2pDnsSdServiceInfo info = WifiP2pDnsSdServiceInfo.newInstance(SERVICE_NAME, SERVICE_TYPE, record);

        mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mWifiP2pManager.initialize(this, getMainLooper(), new WifiP2pManager.ChannelListener() {
            @Override
            public void onChannelDisconnected() {
                addLog("initialize: onChannelDisconnected!");
            }
        });

        mWifiP2pManager.clearLocalServices(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                addLog("clearLocalServices: Success!");
                mWifiP2pManager.addLocalService(mChannel, info, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        addLog("addLocalService: Success!");
                    }

                    @Override
                    public void onFailure(int code) {
                        addLog("addLocalService Error: " + Integer.toString(code));
                    }
                });
            }

            @Override
            public void onFailure(int code) {
                addLog("clearLocalServices Error: " + Integer.toString(code));
            }
        });

        // new Thread(ChatServer.getInstance()).start();







    }

    public void runDiscovery() {

        WifiP2pManager.DnsSdTxtRecordListener txtListener = new WifiP2pManager.DnsSdTxtRecordListener() {
            @Override
            /* Callback includes:
             * fullDomain: full domain name: e.g "printer._ipp._tcp.local."
             * record: TXT record dta as a map of key/value pairs.
             * device: The device running the advertised service.
             */
            public void onDnsSdTxtRecordAvailable(String fullDomain, Map<String, String> record, WifiP2pDevice device) {
                addLog("runDiscovery: DnsSdTxtRecord available - " + record.toString());
                String name = record.get(NAME_RECORD_FIELD);
                if (name != null) {
                    /*
                    Buddy buddy = new Buddy(device.deviceAddress, name);
                    mBuddyListAdapter.putBuddy(buddy);
                    */










                }
            }
        };

        WifiP2pManager.DnsSdServiceResponseListener servListener = new WifiP2pManager.DnsSdServiceResponseListener() {
            @Override
            public void onDnsSdServiceAvailable(String instanceName, String registrationType, WifiP2pDevice resourceType) {
                addLog("DnsSdServiceAvailable: Service " + instanceName);
                if (!instanceName.equals(SERVICE_NAME)) return;
                /*
                Buddy buddy = mBuddyListAdapter.getItem(resourceType.deviceAddress);
                if (buddy != null) {
                    addLog("DnsSdServiceAvailable", buddy.getBuddyName());
                }
                */










            }
        };

        mWifiP2pManager.setDnsSdResponseListeners(mChannel, servListener, txtListener);

        WifiP2pDnsSdServiceRequest serviceRequest = WifiP2pDnsSdServiceRequest.newInstance(SERVICE_NAME, SERVICE_TYPE);
        // WifiP2pDnsSdServiceRequest serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        mWifiP2pManager.addServiceRequest(
                mChannel,
                serviceRequest,
                new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        addLog("ServiceRequest: onSuccess");
                    }

                    @Override
                    public void onFailure(int code) {
                        // Command failed.  Check for P2P_UNSUPPORTED, BUSY, or ERROR
                        switch (code) {
                            case WifiP2pManager.ERROR:
                                addLog("ServiceRequest: Error.");
                                break;
                            case WifiP2pManager.P2P_UNSUPPORTED:
                                addLog("ServiceRequest: P2P isn't supported on this device.");
                                break;
                            case WifiP2pManager.BUSY:
                                addLog("ServiceRequest: Busy.");
                                break;
                            case WifiP2pManager.NO_SERVICE_REQUESTS:
                                addLog("ServiceRequest: No Service request.");
                                break;
                        }
                    }
                }
        );
        mWifiP2pManager.discoverServices(
                mChannel,
                new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        addLog("ServiceDiscovery: onSuccess");
                    }

                    @Override
                    public void onFailure(int code) {
                        switch (code) {
                            case WifiP2pManager.ERROR:
                                addLog("ServiceDiscovery: Error.");
                                break;
                            case WifiP2pManager.P2P_UNSUPPORTED:
                                addLog("ServiceDiscovery: P2P isn't supported on this device.");
                                break;
                            case WifiP2pManager.BUSY:
                                addLog("ServiceDiscovery: Busy.");
                                break;
                            case WifiP2pManager.NO_SERVICE_REQUESTS:
                                addLog("ServiceDiscovery: No Service request.");
                                break;
                        }
                    }
                }
        );
    }



    public void addLog(String str) {
        str = str + logging.getText().toString();
        if (str.length() > LOGLENGTH) {
            logging.setText(str.substring(0, LOGLENGTH));
        }
    }

    private class WifiP2pStateChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                // Determine if Wifi P2P mode is enabled or not, alert the Activity.
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    //activity.setIsWifiP2pEnabled(true);
                    addLog("WIFI_P2P_STATE_ENABLED");
                } else {
                    //activity.setIsWifiP2pEnabled(false);
                    addLog("WIFI_P2P_STATE_DISABLED");
                }
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // The peer list has changed!  We should probably do something about that.
                addLog("Peer list has changed");
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                // Connection state changed!  We should probably do something about that.
                addLog("Connection state changed");
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if (networkInfo.isConnected()) {
                    addLog("Connected!");
                    mWifiP2pManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                        @Override
                        public void onConnectionInfoAvailable(final WifiP2pInfo info) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    addLog("Connection info availabable");
                                    try {
                                        SocketChannel socket = SocketChannel.open();
                                        socket.connect(new InetSocketAddress(info.groupOwnerAddress, LISTEN_PORT));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    });
                } else {
                    addLog("Not connected any more");
                }
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                addLog("This device changed");
            }
        }
    }

}
